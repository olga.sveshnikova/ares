﻿namespace Ares.Web.App.Wrappers
{
	using System.Threading.Tasks;

	public interface IHttpClient
	{
		Task<TModel> GetJsonAsync<TModel>(string path);

		Task<TModel> PostJsonAsync<TModel>(string path, object content);

		Task<TModel> PutJsonAsync<TModel>(string path, object content);

		Task DeleteAsync(string path);

		string GetUrl(string path);
	}
}