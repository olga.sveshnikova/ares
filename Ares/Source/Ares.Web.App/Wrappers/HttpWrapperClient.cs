﻿namespace Ares.Web.App.Wrappers
{
    using Microsoft.AspNetCore.Components;
    using System;
	using System.Diagnostics.CodeAnalysis;
	using System.Net.Http;
	using System.Threading.Tasks;

	[ExcludeFromCodeCoverage]
	public class HttpClientWrapper
		: IHttpClient
	{
		private const string ApiUrl = "http://localhost:55154";

		private HttpClient httpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpClientWrapper"/> class.
		/// </summary>
		public HttpClientWrapper()
		{
			this.httpClient = new HttpClient();
		}

		public async Task<TModel> GetJsonAsync<TModel>(string path)
		{
			return await this.httpClient.GetJsonAsync<TModel>(this.GetUrl(path));
		}

		public async Task<TModel> PostJsonAsync<TModel>(string path, object content)
		{
			return await this.httpClient.PostJsonAsync<TModel>(this.GetUrl(path), content);
		}

		public async Task<TModel> PutJsonAsync<TModel>(string path, object content)
		{
			return await this.httpClient.PutJsonAsync<TModel>(this.GetUrl(path), content);
		}

		public async Task DeleteAsync(string path)
		{
			await this.httpClient.DeleteAsync(this.GetUrl(path));
		}

		public string GetUrl(string path)
		{
			Uri api = new Uri(ApiUrl);
			Uri uri = new Uri(api, path);

			return uri.ToString();
		}
	}
}