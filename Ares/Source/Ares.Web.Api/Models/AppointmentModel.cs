﻿namespace Ares.Web.Api.Models
{
	public class AppointmentModel
	{
		private Appointment appointment;
		private Car car;
		private CarOwner carOwner;
		private Damage damage;

		public Appointment Appointment
		{
			get => this.appointment ??= new Appointment();
			set => this.appointment = value;
		}

		public Car Car
		{
			get => this.car ??= new Car();
			set => this.car = value;
		}

		public CarOwner CarOwner
		{
			get => this.carOwner ??= new CarOwner();
			set => this.carOwner = value;
		}

		public Damage Damage
		{
			get => this.damage ??= new Damage();
			set => this.damage = value;
		}

		//public IList<Damage> ListOfDamages { get; set; }
	}
}