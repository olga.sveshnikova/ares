﻿namespace Ares.Web.Api.Models
{
	public class CarOwner
	{
		public string Firstname { get; set; }

		public string Lastname { get; set; }

		public string ZIP { get; set; }

		public string City { get; set; }

		public string Street { get; set; }

		public string Number { get; set; }

		public string PhoneNumber { get; set; }
	}
}