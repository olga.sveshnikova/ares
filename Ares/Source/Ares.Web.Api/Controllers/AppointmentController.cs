﻿namespace Ares.Web.Api.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using Ares.Web.Api.Models;

	using Microsoft.AspNetCore.Mvc;

	[Route("api/appointments")]
	public class AppointmentController : ControllerBase
	{
		private static List<AppointmentModel> listOfAppointments = new List<AppointmentModel>();

		[Route("")]
		[HttpGet]
		public IEnumerable<AppointmentModel> GetAppointments()
		{
			if (!listOfAppointments.Any())
			{
				AppointmentModel firstAppointment = new AppointmentModel();
				firstAppointment.Appointment = new Appointment();
				firstAppointment.Appointment.Id = Guid.NewGuid();
				firstAppointment.Appointment.Date = DateTime.Parse("2020-05-08 10:00");
				firstAppointment.Damage = new Damage();
				firstAppointment.Damage.DamageDescription = "Lampe vorne links ist kaputt";
				firstAppointment.Car = new Car();
				firstAppointment.Car.KFZ = "HB-TN202";
				firstAppointment.CarOwner = new CarOwner();
				firstAppointment.CarOwner.Firstname = "Max";
				firstAppointment.CarOwner.Lastname = "Mustermann";

				AppointmentModel secondAppointment = new AppointmentModel();
				secondAppointment.Appointment = new Appointment();
				secondAppointment.Appointment.Id = Guid.NewGuid();
				secondAppointment.Appointment.Date = DateTime.Parse("2020-05-12 14:00");
				secondAppointment.Damage = new Damage();
				secondAppointment.Damage.DamageDescription = "Beifahrertür muss lakiert sein";
				secondAppointment.Car = new Car();
				secondAppointment.Car.KFZ = "H-FK202";
				secondAppointment.CarOwner = new CarOwner();
				secondAppointment.CarOwner.Firstname = "Mike";
				secondAppointment.CarOwner.Lastname = "Lowrey";

				listOfAppointments.Add(firstAppointment);
				listOfAppointments.Add(secondAppointment);
			}

			return listOfAppointments;
		}

		[Route("details/{id:guid}")]
		[HttpGet]
		public AppointmentModel GetAppointment(Guid id)
		{
			return listOfAppointments.First(a => a.Appointment.Id == id);
		}

		[Route("save")]
		[HttpPost]
		public void InsertNewAppointment([FromBody] AppointmentModel model)
		{
			AppointmentModel newAppointment = new AppointmentModel
											  {
												  Appointment = new Appointment { Date = model.Appointment.Date, Id = Guid.NewGuid() },

												  Car = new Car { KFZ = model.Car.KFZ },

												  Damage = new Damage { DamageDescription = model.Damage.DamageDescription },

												  CarOwner = new CarOwner
															 {
																 Firstname = model.CarOwner.Firstname,
																 Lastname = model.CarOwner.Lastname,
																 Street = model.CarOwner.Street,
																 Number = model.CarOwner.Number,
																 ZIP = model.CarOwner.ZIP,
																 City = model.CarOwner.City,
																 PhoneNumber = model.CarOwner.PhoneNumber
															 }
											  };

			listOfAppointments.Add(newAppointment);
		}
	}
}